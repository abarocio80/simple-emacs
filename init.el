(load-theme 'modus-operandi t)
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(column-number-mode 1)

(set-face-attribute 'default nil
                    :height 160)
(setq visible-bell t)

(global-set-key (kbd "<f5>") #'modus-themes-toggle)1

(defun flex-if-twiddle (pattern index total)
  (when (string-suffix-p "~" pattern)
    `(orderless-flex . ,(substring pattern 0 -1))))

(defun first-initialism (pattern index total)
  (if (= index 0) 'orderless-initialism))

(defun without-if-bang (pattern index total)
  (cond
   ((equal "!" pattern)
    '(orderless-literal . ""))
   ((string-prefix-p "!" pattern)
    `(orderless-without-literal . ,(substring pattern 1)))))

(package-initialize)
(package-install 'marginalia)
(marginalia-mode 1)
;; (package-install 'mct) ; need to investigate
(package-install 'vertico)
(vertico-mode 1)
(package-install 'orderless)
(setq completion-styles '(orderless)
      orderless-matching-styles '(orderless-regexp)
      orderless-style-dispatchers '(;;first-initialism
				    ;;flex-if-twiddle
				    without-if-bang))

(package-install 'consult)

(package-install 'embark)
(define-key minibuffer-mode-map (kbd "C-a") #'embark-act)
(define-key minibuffer-mode-map (kbd "C-e") #'embark-export)
(define-key minibuffer-mode-map (kbd "C-c") #'embark-collect)
(define-key minibuffer-mode-map (kbd "C-b") #'embark-become)

(package-install 'cape)

